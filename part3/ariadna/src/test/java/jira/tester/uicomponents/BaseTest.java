package jira.tester.uicomponents;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public abstract class BaseTest {

//	  public static WebDriver driver = new FirefoxDriver();
	  public static WebDriver driver;
	  
	  @BeforeClass
	  public static void setup() {
		driver = new FirefoxDriver();
	    driver.get("http://jira.atlassian.com/browse/TST");
	  }

	
	@AfterClass
	public static void tearDown() {
		driver.quit();
	}

}
