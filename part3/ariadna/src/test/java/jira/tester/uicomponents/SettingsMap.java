package jira.tester.uicomponents;

import java.io.IOException;
import java.util.Properties;

public class SettingsMap {
	private static SettingsMap instance = new SettingsMap();
	
	private Properties properties = new Properties();
	
	private SettingsMap(){
		try {
			properties.load(SettingsMap.class.getResourceAsStream("/settings.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static String find(String entryID){
		return instance.properties.getProperty(entryID);
	}
}