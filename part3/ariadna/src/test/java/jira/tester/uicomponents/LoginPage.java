package jira.tester.uicomponents;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends LoadableComponent<LoginPage> {
	
	private static WebDriver driver;
	
	public LoginPage(WebDriver webDriver) {
		driver = webDriver;
	}

	public static void typeUsername(String username) {
		driver.findElement(By.id(SettingsMap.find("username_id"))).sendKeys(username);
	}

	public static void typePassword(String password) {
		driver.findElement(By.id(SettingsMap.find("password_id"))).sendKeys(password);
	}

	public static SummaryPage submitLogin() {
		driver.findElement(By.id(SettingsMap.find("loginSubmit_id"))).click();
		return new SummaryPage(driver).get();
	}

	public SummaryPage submitLoginExpectingFailure() {
		driver.findElement(By.id("login-submit")).click();
		return new SummaryPage(driver).get();
	}

	public SummaryPage loginAs(String username, String password) {
		typeUsername(username);
		typePassword(password);
		return submitLogin();
	}

	@Override
	protected void load() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void isLoaded() throws Error {

	     ExpectedCondition<Boolean> expectation = new
	    ExpectedCondition<Boolean>() {
	        public Boolean apply(WebDriver driver) {
	          return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
	        }
	      };

	     Wait<WebDriver> wait = new WebDriverWait(driver, 5);
	      try {
	              wait.until(expectation);
	      } catch(Throwable error) {
	              Assert.assertFalse("Timeout waiting for Page Load Request to complete.",true);
	      }
	 }
}
