package jira.tester.uicomponents;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageCommon extends LoadableComponent<PageCommon> {

	 private WebDriver driver;

	public PageCommon(WebDriver webDriver) {
		driver = webDriver;
	}
	 
	public void loginLinkClick() {
		String loginCssSelector = "li#user-options > a";
		driver.findElement(By.cssSelector(loginCssSelector)).click();
	}

	public void selectFromTopMenu(String topMenuSection,
			String sectionName) {

		driver.findElement(By.id(topMenuSection)).click();
		// driver.findElement(By.xpath("//a[contains(@id, '" + fromDropDownName
		// + "') and contains(@title, '" + title + "')]")).click();
		// driver.findElement(By.name(sectionName)).click();
		driver.findElement(
				By.xpath("//a[contains(text(), '" + sectionName + "')]"))
				.click();
	}

	public boolean checkYourCurrentUrl(String checkUrl) {

		assertEquals("The URL values are not equal!", checkUrl,
				driver.getCurrentUrl());

		return false;
	}

	@Override
	protected void load() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void isLoaded() throws Error {

		ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript(
						"return document.readyState").equals("complete");
			}
		};

		Wait<WebDriver> wait = new WebDriverWait(driver, 5);
		try {
			wait.until(expectation);
		} catch (Throwable error) {
			Assert.assertFalse(
					"Timeout waiting for Page Load Request to complete.", true);
		}
	}
}
