package jira.tester.uicomponents;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CreateIssue {

	private WebDriver driver;
	
	public CreateIssue(WebDriver webDriver) {
		driver = webDriver;
	}

	public SummaryPage createIssueSubmit() {
		driver.findElement(By.id(SettingsMap.find("createSubmit_id"))).click();
		return new SummaryPage(driver).get();
	}

	public void typeSummary(String summary) {
		driver.findElement(By.id("summary")).sendKeys(summary);
	}

	public SummaryPage createNewFeatureIssue(String summary) {
		typeSummary(summary);
		return createIssueSubmit();
	}
	public SummaryPage createNewIssue(String issueType, String summary) {
		issueType(issueType);
		typeSummary(summary);
		return createIssueSubmit();
	}

	private void issueType(String issueType) {
		// TODO Auto-generated method stub
		
	}
}
