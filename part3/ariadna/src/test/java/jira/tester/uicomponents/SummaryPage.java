package jira.tester.uicomponents;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SummaryPage extends LoadableComponent<SummaryPage>{
	
	private WebDriver driver;
	
	public SummaryPage(WebDriver webDriver) {
		driver = webDriver;
	}
	
	public CreateIssue createIssueClick() {	
		driver.findElement(By.id(SettingsMap.find("createIssue_id"))).click();
		
			new WebDriverWait(driver, 10)
			  .until(ExpectedConditions.visibilityOf(driver.findElement(By.id("create-issue-dialog"))));
			
			return new CreateIssue(driver);
	}
	
	public void searchIssue(String issueID) {
		 driver.findElement(By.id(SettingsMap.find("searchField_id"))).sendKeys(issueID);
		
	}
	
	public String getIssueID(String bugTitle){
		return driver.findElement(By.xpath("//a[contains(text(), '" + bugTitle + "')]"))
				.getText().split(" - ")[0];
	}

	@Override
	protected void load() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void isLoaded() throws Error {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Planned for future
//	     ExpectedCondition<Boolean> expectation = new
//	    ExpectedCondition<Boolean>() {
//	        public Boolean apply(WebDriver driver) {
//	          return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
//	        }
//	      };
//
//	     Wait<WebDriver> wait = new WebDriverWait(driver, 10);
//	      try {
//	              wait.until(expectation);
//	      } catch(Throwable error) {
//	              Assert.assertFalse("Timeout waiting for Page Load Request to complete.",true);
//	      }
		
	}


}
