package jira.tests;

import jira.tester.uicomponents.BaseTest;
import jira.tester.uicomponents.CreateIssue;
import jira.tester.uicomponents.LoginPage;
import jira.tester.uicomponents.PageCommon;
import jira.tester.uicomponents.SettingsMap;
import jira.tester.uicomponents.SummaryPage;

import org.junit.Test;

public class CreateIssueTest extends BaseTest {

	/**
	 * This is about <code>test01_JIRALogIn</code>.<br>
	 * <b>Description:</b> Login to jira with correct user/pass and create new Issue<br>
	 * 
	 * @MyReturn new issue is created <br>
	 * 
	 * 
	 */
	@Test
	public void test_createNewIssue() {
		
		String bugTitle = "This is a summary of a new bug";
	
		PageCommon pageCommon = new PageCommon(driver);
		pageCommon.loginLinkClick();
		LoginPage loginPage = new LoginPage(driver);
		SummaryPage summaryPage = loginPage.loginAs(SettingsMap.find("user"), SettingsMap.find("password"));
		CreateIssue createIssue = summaryPage.createIssueClick();
		createIssue.createNewFeatureIssue(bugTitle);
		String issueID = summaryPage.getIssueID(bugTitle);
		summaryPage.searchIssue(issueID);
		//etc
		
	}
}
