/*my thoughts
here I tried to rephrase what requirements are

                                             We have to add Watcher to all issues.
                                             Some issues cannot be modified, so new Watcher cannot be added,
                                             so such issues will be collected and Return list of issues which cannot be modified.
                                            
                                             We have list of issues.
                                             We take list and work with each issue one by one.
                                             if issue can be modified, then add watcher and next
                                                            else add to the LastList


here is my solution:
*/
    public ArrayList<Issue> addWatcherToAll(final ArrayList<Issue> issues, final User currentUser, final User watcher) {
        ArrayList<Issue> successfulIssues = new ArrayList<Issue>();
        ArrayList<Issue> failedIssues = new ArrayList<Issue>();
        for (Issue issue : issues) {
            if (canWatchIssue(issue, currentUser, watcher)) {
                successfulIssues.add(issue);
            } else {
                failedIssues.add(issue);
            }
        }
        if (!successfulIssues.isEmpty()) {
            watcherManager.startWatching(watcher, successfulIssues);
        }
        return failedIssues;
    }

    private boolean canWatchIssue(Issue issue, User currentUser, User watcher) {
        if (currentUser.getHasPermissionToModifyWatchers()) {
            return issue.getWatchingAllowed(watcher);
        }
        return false;
    }

	/*
I modified 4 points:
1. last row - changed from return true to return false
2. removed currentUser.equals(watcher) || --as there are no requirements which says that current user has rights not modify watchers for himself
3. modified         if (successfulIssues.isEmpty()) {
to         if (!successfulIssues.isEmpty()) {
as it doesn't make sense to work with empty list
4. modified             watcherManager.startWatching(currentUser, successfulIssues);
to             watcherManager.startWatching(watcher, successfulIssues);
as I dont understand why manager want to add currentUser if the goal is to add watcher to the list (not necessary currentUser)
*/